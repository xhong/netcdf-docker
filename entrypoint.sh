#!/bin/bash

set -e

. pyenv/bin/activate

printf "exec %s \n\n" "$@"
exec "$@"
